# Barrow's Pizza
This is a secret family recipe that could use some tweaking.

## Toppings
1. Peppers
2. Olives
3. Anchovies 
4. Sausage
5. Pepperoni
6. Kale

## Pizza Lab

### Crust
1. Cheesey Flour
 * TODO: find a way to infuse the flour with cheese
2. Burrito Crust
 * possibly fried
 * TODO: research Chimichangas
